//
//  ForwardingTarget.h
//  UIResponseTest
//
//  Created by tl on 2019/1/10.
//  Copyright © 2019 tl. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ForwardingTarget : NSObject

@end

NS_ASSUME_NONNULL_END
