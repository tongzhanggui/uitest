//
//  Monkey.m
//  UIResponseTest
//
//  Created by tl on 2019/1/10.
//  Copyright © 2019 tl. All rights reserved.
//

#import "Monkey.h"
#import "ForwardingTarget.h"
#import <objc/runtime.h>

@interface Monkey ()

@property(nonatomic,strong)ForwardingTarget *target;

@end

@implementation Monkey

- (instancetype)init
{
    self = [super init];
    if (self) {
        _target = [[ForwardingTarget alloc]init];
        [self performSelector:@selector(sel:) withObject:@"yeyu"];
    }
    return self;
}

id dynamicMethodIMP(id self,SEL _cmd,NSString *str)
{
    NSLog(@"%s:动态添加的方法",__FUNCTION__);
    NSLog(@"%@", str);
    return @"1";
    
}

+(BOOL)resolveInstanceMethod:(SEL)sel{
    class_addMethod([self class], sel, (IMP)dynamicMethodIMP, "@@:");
    BOOL result = [super resolveInstanceMethod:sel];
    return result;
    
    
    
}

-(id)forwardingTargetForSelector:(SEL)aSelector
{
//    id result = [super forwardingTargetForSelector:aSelector];
//    result = self.target;
//    return result;

    return self.target;
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector{
//    id result = [super methodSignatureForSelector:aSelector];
    NSMethodSignature *sig = [NSMethodSignature signatureWithObjCTypes:"v@:"];
    
    return sig;
}

- (void)forwardInvocation:(NSInvocation *)anInvocation{
    anInvocation.selector = @selector(invocationTest);
    [self.target forwardInvocation:anInvocation];
}


@end
