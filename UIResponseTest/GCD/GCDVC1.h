//
//  GCDVC1.h
//  UIResponseTest
//
//  Created by tl on 2018/11/30.
//  Copyright © 2018 tl. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GCDVC1 : UIViewController

// 加 class 是类方法
@property(class,nonatomic,copy)NSString *str;

@property(class,nonatomic,readonly)GCDVC1 *currentGCD;

@end

NS_ASSUME_NONNULL_END
