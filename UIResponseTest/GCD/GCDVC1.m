//
//  GCDVC1.m
//  UIResponseTest
//
//  Created by tl on 2018/11/30.
//  Copyright © 2018 tl. All rights reserved.
//

#import "GCDVC1.h"

@interface GCDVC1 ()



@end

@implementation GCDVC1

//+(BOOL)resolveInstanceMethod:(SEL)sel{
//
//      
//    return YES;
//}
//
//-(id)forwardingTargetForSelector:(SEL)aSelector{
//
//}
//
//-(NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector{
//    NSMethodSignature *sig = [super methodSignatureForSelector:aSelector];
//    return sig;
//}
//
//-(void)forwardInvocation:(NSInvocation *)anInvocation{
//
//}

+ (GCDVC1 *)currentGCD{
    GCDVC1 *gcd = [[GCDVC1 alloc]init];
    return gcd;
}

+ (NSString *)str{
    
    return @"aaaaa";
}

+(void)setStr:(NSString *)str{
    
    NSLog(@"bbbbb");
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [NSString superclass];
    
    [NSOperationQueue mainQueue];
    
    [GCDVC1 str];
    GCDVC1.str = @"aaa";
    
    NSOperationQueue *q = NSOperationQueue.mainQueue;
    
    
    
    [self apply:nil];
    
    
    
    
    // Do any additional setup after loading the view.
}

- (IBAction)gcd1:(id)sender {
    dispatch_queue_t queue = dispatch_queue_create("apply", DISPATCH_QUEUE_CONCURRENT);
    
        dispatch_apply(10, queue, ^(size_t index) {
            NSLog(@"%zd-----------%@",index,[NSThread currentThread]);
        });
    
    dispatch_async(queue, ^{
        for (NSInteger i = 0; i<10; i++) {
            NSLog(@"%zd-download1--%@",i,[NSThread currentThread]);
        }
        
    });
    
    dispatch_async(queue, ^{
        for (NSInteger i = 0; i<10; i++) {
            NSLog(@"%zd-download2--%@",i,[NSThread currentThread]);
        }
        
    });
    
    dispatch_async(queue, ^{
        for (NSInteger i = 0; i<10; i++) {
            NSLog(@"%zd-download3--%@",i,[NSThread currentThread]);
        }
        
    });
    
    dispatch_sync(queue, ^{
        for (NSInteger i = 0; i<10; i++) {
            NSLog(@"同步执行%zd-download--%@",i,[NSThread currentThread]);
        }
    });
    
    
    dispatch_sync(queue, ^{
        NSLog(@"我是一个栅栏函数2");
    });
    
    dispatch_sync(queue, ^{
        NSLog(@"我是一个栅栏函数3");
    });
    //    dispatch_barrier_sync(queue, ^{
    //
    //        NSLog(@"我是一个栅栏函数");
    //
    //    });
    
    dispatch_async(queue, ^{
        for (NSInteger i = 0; i<10; i++) {
            NSLog(@"%zd-download4--%@",i,[NSThread currentThread]);
        }
        
    });
}


- (IBAction)operation:(id)sender {
    
    
    
//
//    NSInvocationOperation *op = [[NSInvocationOperation alloc]initWithTarget:self selector:@selector(task1) object:nil];
//
//    [op start];
//
    
    NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^{
        for (int i = 0; i<2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"1----%@",[NSThread currentThread]);
        }
    }];
    
    [op addExecutionBlock:^{
        for (int i = 0; i<2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"2----%@",[NSThread currentThread]);
        }
    }];
    
    [op addExecutionBlock:^{
        for (int i = 0; i<2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"3----%@",[NSThread currentThread]);
        }
    }];
    
    [op addExecutionBlock:^{
        for (int i = 0; i<2; i++) {
            [NSThread sleepForTimeInterval:2];
            NSLog(@"4----%@",[NSThread currentThread]);
        }
    }];
    
    [op start];
}

- (void)task1{
    
    for (int i = 0; i < 2; i++) {
        [NSThread sleepForTimeInterval:2]; // 模拟耗时操作
        NSLog(@"1---%@", [NSThread currentThread]); // 打印当前线程
    }
    
}
- (IBAction)dependency:(id)sender {
    
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    
    
    
    NSBlockOperation *op1 = [NSBlockOperation blockOperationWithBlock:^{
        [NSThread sleepForTimeInterval:2]; // 模拟耗时操作
        NSLog(@"1---%@", [NSThread currentThread]); // 打印当前线程
    }];
    
    NSBlockOperation *op2 = [NSBlockOperation blockOperationWithBlock:^{
        [NSThread sleepForTimeInterval:2]; // 模拟耗时操作
        NSLog(@"2---%@", [NSThread currentThread]); // 打印当前线程
    }];
    
    NSBlockOperation *op3 = [NSBlockOperation blockOperationWithBlock:^{
        [NSThread sleepForTimeInterval:6]; // 模拟耗时操作
        NSLog(@"3---%@", [NSThread currentThread]); // 打印当前线程
    }];
    
    NSBlockOperation *op4 = [NSBlockOperation blockOperationWithBlock:^{
        [NSThread sleepForTimeInterval:2]; // 模拟耗时操作
        NSLog(@"4---%@", [NSThread currentThread]); // 打印当前线程
    }];
    
    NSBlockOperation *op5 = [NSBlockOperation blockOperationWithBlock:^{
        [NSThread sleepForTimeInterval:6]; // 模拟耗时操作
        NSLog(@"5---%@", [NSThread currentThread]); // 打印当前线程
    }];
    
    op5.queuePriority = NSOperationQueuePriorityLow;
    
    
    [queue addOperations:@[op1,op2,op3] waitUntilFinished:YES];
    [queue addOperation:op4];
    [queue addOperation:op5];
    
    
    
}


/**
 * 异步执行 + 主队列
 * 特点：只在主线程中执行任务，执行完一个任务，再执行下一个任务
 */
- (IBAction)asyncMain:(id)sender {
    NSLog(@"currentThread---%@",[NSThread currentThread]);  // 打印当前线程
    NSLog(@"asyncMain---begin");
    
    dispatch_queue_t queue = dispatch_get_main_queue();
    
    dispatch_async(queue, ^{
        // 追加任务1
        for (int i = 0; i < 2; ++i) {
            [NSThread sleepForTimeInterval:2];              // 模拟耗时操作
            NSLog(@"1---%@",[NSThread currentThread]);      // 打印当前线程
        }
    });
    
    dispatch_async(queue, ^{
        // 追加任务2
        for (int i = 0; i < 2; ++i) {
            [NSThread sleepForTimeInterval:2];              // 模拟耗时操作
            NSLog(@"2---%@",[NSThread currentThread]);      // 打印当前线程
        }
    });
    
    dispatch_async(queue, ^{
        // 追加任务3
        for (int i = 0; i < 2; ++i) {
            [NSThread sleepForTimeInterval:2];              // 模拟耗时操作
            NSLog(@"3---%@",[NSThread currentThread]);      // 打印当前线程
        }
    });
    
    NSLog(@"asyncMain---end");
    
    
}

- (IBAction)apply:(id)sender {
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    NSLog(@"apply---begin");
    dispatch_apply(6, queue, ^(size_t index) {
        NSLog(@"%zd---%@",index, [NSThread currentThread]);
    });
    NSLog(@"apply---end");
    

}
- (IBAction)groupEnterAndLeave:(id)sender {
    
    
    NSLog(@"currentThread-----%@",[NSThread currentThread]);
    NSLog(@"group---begin");
    
    dispatch_group_t group = dispatch_group_create();
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    
    dispatch_group_enter(group);
    dispatch_async(queue, ^{
        for (int i = 0; i < 2; ++i) {
            [NSThread sleepForTimeInterval:2];              // 模拟耗时操作
            NSLog(@"1---%@",[NSThread currentThread]);      // 打印当前线程
        }
        dispatch_group_leave(group);
    });
    
    dispatch_group_enter(group);
    dispatch_async(queue, ^{
        // 追加任务2
        for (int i = 0; i < 2; ++i) {
            [NSThread sleepForTimeInterval:2];              // 模拟耗时操作
            NSLog(@"2---%@",[NSThread currentThread]);      // 打印当前线程
        }
        dispatch_group_leave(group);
    });
    

    dispatch_group_notify(group, queue, ^{
        // 等前面的异步操作都执行完毕后，回到主线程.
        for (int i = 0; i < 2; ++i) {
            [NSThread sleepForTimeInterval:2];              // 模拟耗时操作
            NSLog(@"3---%@",[NSThread currentThread]);      // 打印当前线程
        }
        NSLog(@"group---end");

    });
    
//    NSLog(@"currentThread-----%@",[NSThread currentThread]); //
//    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    

    
}
- (IBAction)semaphore:(id)sender {
    
    NSLog(@"currentThread---%@",[NSThread currentThread]);  // 打印当前线程
    NSLog(@"semaphore---begin");
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    __block int number = 0;
    dispatch_async(queue, ^{
        // 追加任务1
        [NSThread sleepForTimeInterval:2];              // 模拟耗时操作
        NSLog(@"1---%@",[NSThread currentThread]);      // 打印当前线程
        
        number = 100;
        
        dispatch_semaphore_signal(semaphore);
    });
    
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    NSLog(@"semaphore---end,number = %zd",number);
    
    
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
