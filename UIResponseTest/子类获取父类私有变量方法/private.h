
//
//  private.h
//  UIResponseTest
//
//  Created by tl on 2018/11/28.
//  Copyright © 2018 tl. All rights reserved.
//

#ifndef private_h
#define private_h


@interface Father ()

@property (nonatomic, copy) NSString *privateThingSonNeed;
- (void)privateMethodNeedsSonOverride;

@end

#endif /* private_h */
