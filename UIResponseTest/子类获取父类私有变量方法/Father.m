//
//  Father.m
//  UIResponseTest
//
//  Created by tl on 2018/11/28.
//  Copyright © 2018 tl. All rights reserved.
//

#import "Father.h"
#import "private.h"

@implementation Father

- (instancetype)init
{
    self = [super init];
    if (self) {
        _privateThingSonNeed = @"";
    }
    return self;
}

@end
