//
//  Son.h
//  UIResponseTest
//
//  Created by tl on 2018/11/28.
//  Copyright © 2018 tl. All rights reserved.
//

#import "Father.h"

NS_ASSUME_NONNULL_BEGIN

@interface Son : Father

@end

NS_ASSUME_NONNULL_END
