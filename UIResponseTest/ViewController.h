//
//  ViewController.h
//  UIResponseTest
//
//  Created by tl on 2018/8/2.
//  Copyright © 2018年 tl. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^HandlerComplete)(id response);

@interface ViewController : UIViewController


@end

