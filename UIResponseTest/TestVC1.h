//
//  TestVC1.h
//  UIResponseTest
//
//  Created by tl on 2018/11/6.
//  Copyright © 2018年 tl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TestVC1Delegate

- (int)add;

@end

typedef void(^test)(id response);


@interface TestVC1 : UIViewController

@property(nonatomic,weak)id<TestVC1Delegate> delegate;

- (void)testBlock:(test)block;

- (void)block:(void(^)(id res))block;







@end


