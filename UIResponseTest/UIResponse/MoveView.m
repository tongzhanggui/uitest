//
//  MoveView.m
//  UIResponseTest
//
//  Created by tl on 2018/8/2.
//  Copyright © 2018年 tl. All rights reserved.
//

#import "MoveView.h"


@implementation MoveView

//- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
//{
//    NSLog(@"move");
//
//    NSLog(@"move===%d",[self pointInside:point withEvent:event]);
//
//    return nil;
//}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSLog(@"开始触摸");
    
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (touches.count > 1) {
        return;
    }
    UITouch *touch = [touches anyObject];
    
    CGPoint curP = [touch locationInView:self];
    // 获取上一个点的位置
    CGPoint preP = [touch previousLocationInView:self];
//    NSLog(@"%f,%f   %f,%f",curP.x,curP.y,preP.x,preP.y);
    
    // 获取它们x轴的偏移量,每次都是相对上一次
    CGFloat offsetX = curP.x - preP.x;
    // 获取y轴的偏移量
    CGFloat offsetY = curP.y - preP.y;
    // 修改控件的形变或者frame,center,就可以控制控件的位置
    // 形变也是相对上一次形变(平移)
    // CGAffineTransformMakeTranslation:会把之前形变给清空,重新开始设置形变参数
    // make:相对于最原始的位置形变
    // CGAffineTransform t:相对这个t的形变的基础上再去形变
    // 如果相对哪个形变再次形变,就传入它的形变
    
    
    self.transform = CGAffineTransformTranslate(self.transform, offsetX, offsetY);
    
    
        if (self.frame.origin.x < 0) {
            self.frame = CGRectMake(0, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
        }
        if(self.frame.origin.y < TL_StatusBarAndNavigationBarHeight){
            self.frame = CGRectMake(self.frame.origin.x,TL_StatusBarAndNavigationBarHeight , self.frame.size.width, self.frame.size.height);
        }
        if((self.frame.origin.x+self.frame.size.width)>[UIScreen mainScreen].bounds.size.width){
            self.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-self.frame.size.width,self.frame.origin.y, self.frame.size.width, self.frame.size.height);
        }
        if((self.frame.origin.y+self.frame.size.height)>[UIScreen mainScreen].bounds.size.height){
            self.frame = CGRectMake(self.frame.origin.x,[UIScreen mainScreen].bounds.size.height-self.frame.size.height, self.frame.size.width, self.frame.size.height);
        }
    
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"结束触摸");
}

// 触摸结束前，某个系统事件(例如电话呼入)会打断触摸过程，系统会自动调用view的下面方法
- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"cancle");
}

- (void)pressesBegan:(NSSet<UIPress *> *)presses withEvent:(UIPressesEvent *)event
{
    NSLog(@"anya");
}

- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    NSLog(@"shoushi");
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


void exampleC_addBlockToArray(NSMutableArray *array) {
    [array addObject:^{
        printf("Cn");
    }];
}

void exampleC() {
    NSMutableArray *array = [NSMutableArray array];
    exampleC_addBlockToArray(array);
    void (^block)() = [array objectAtIndex:0];
    block();
}


typedef void (^dBlock)();

dBlock exampleD_getBlock() {
    char d = 'D';
    return ^{
        printf("%cn", d);
    };
}

void exampleD() {
    exampleD_getBlock()();
}


typedef void (^eBlock)();

eBlock exampleE_getBlock() {
    char e = 'E';
    void (^block)() = ^{
        printf("%cn", e);
    };
    return block;
}

void exampleE() {
    eBlock block = exampleE_getBlock();
    block();
}



@end
