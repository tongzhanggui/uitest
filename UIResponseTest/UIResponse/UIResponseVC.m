//
//  UIResponseVC.m
//  UIResponseTest
//
//  Created by tl on 2019/1/10.
//  Copyright © 2019 tl. All rights reserved.
//

#import "UIResponseVC.h"
#import "MoveView.h"
#import "TestView.h"
#import "Test2View.h"

@interface UIResponseVC ()

@end

@implementation UIResponseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    MoveView *view = [[MoveView alloc]initWithFrame:CGRectMake(10, 100, 100, 100)];
    view.backgroundColor = [UIColor redColor];
    [self.view addSubview:view];
    
    
    CGRect boud = CGRectMake(-30, 0, 100, 100);
    view.bounds = boud;
    
    //    TestView *view1 = [[TestView alloc]initWithFrame:CGRectMake(200,200, 50, 50)];
    //    view1.backgroundColor = [UIColor blackColor];
    //    [self.view addSubview:view1];
    //
    //    Test2View *view2 = [[Test2View alloc]initWithFrame:CGRectMake(0,0, 50, 50)];
    //    view2.backgroundColor = [UIColor blackColor];
    //    [view addSubview:view2];
    //
    //    NSLog(@"bounds");
    //    NSLog(@"x = %f",view1.bounds.origin.x);
    //    NSLog(@"y = %f",view1.bounds.origin.y);
    //    NSLog(@"width = %f",view1.bounds.size.width);
    //    NSLog(@"height = %f",view1.bounds.size.height);
    //
    //    NSLog(@"frame");
    //    NSLog(@"x = %f",view1.frame.origin.x);
    //    NSLog(@"y = %f",view1.frame.origin.y);
    //    NSLog(@"width = %f",view1.frame.size.width);
    //    NSLog(@"height = %f",view1.frame.size.height);
    
    
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
