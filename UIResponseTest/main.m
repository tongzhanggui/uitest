//
//  main.m
//  UIResponseTest
//
//  Created by tl on 2018/8/2.
//  Copyright © 2018年 tl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

