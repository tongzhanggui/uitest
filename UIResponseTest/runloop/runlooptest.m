//
//  runlooptest.m
//  UIResponseTest
//
//  Created by tl on 2018/10/31.
//  Copyright © 2018年 tl. All rights reserved.
//

#import "runlooptest.h"
#import <CoreFoundation/CoreFoundation.h>
#import <UIKit/UIKit.h>
#import <objc/runtime.h>

@implementation runlooptest


- (void)loop{
    
    
//    objc_setAssociatedObject(self, "", self, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//    objc_getAssociatedObject(self, "");
    
    
    
    
    dispatch_queue_t queue = dispatch_queue_create("queue", DISPATCH_QUEUE_CONCURRENT);
    dispatch_async(queue, ^{
        
        [[NSThread currentThread] setName:@"runlooptest"];
        
        NSRunLoop *runloop = [NSRunLoop currentRunLoop];
//        [runloop addPort:[NSMachPort port] forMode:NSRunLoopCommonModes];
        
//        [runloop runUntilDate:[NSDate dateWithTimeIntervalSinceNow:10]];
        
        __block int a = 0;
        NSTimer *time = [NSTimer timerWithTimeInterval:1.f repeats:YES block:^(NSTimer * _Nonnull timer) {
            NSLog(@"---------------------%d---------------------------",a);
            
            a++;
            
            if (a == 5) {
                [timer invalidate];
                
//                CFRunLoopStop(CFRunLoopGetCurrent());
            }
        }];
        
        
        
        
        [runloop addTimer:time forMode:NSRunLoopCommonModes];

        [runloop run];
        
        NSLog(@"yituichu");
    });
    
    
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
    });
    
    
}

@end

