//
//  runlooptest.h
//  UIResponseTest
//
//  Created by tl on 2018/10/31.
//  Copyright © 2018年 tl. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface runlooptest : NSObject
- (void)loop;
@end

NS_ASSUME_NONNULL_END
