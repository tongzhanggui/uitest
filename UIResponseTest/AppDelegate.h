//
//  AppDelegate.h
//  UIResponseTest
//
//  Created by tl on 2018/8/2.
//  Copyright © 2018年 tl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

