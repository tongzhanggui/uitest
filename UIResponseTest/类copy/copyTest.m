//
//  copyTest.m
//  UIResponseTest
//
//  Created by tl on 2018/10/24.
//  Copyright © 2018年 tl. All rights reserved.
//

#import "copyTest.h"
#import <CoreText/CoreText.h>

@interface copyTest (){
    NSMutableSet *_friends;
}

@end

@implementation copyTest

- (void)setName:(NSString * _Nonnull)name
{
    _name = [name copy];
    
}

+(BOOL)resolveClassMethod:(SEL)sel{
    
    
    return YES;
}
- (id)forwardingTargetForSelector:(SEL)aSelector{
    
    id result = [super forwardingTargetForSelector:aSelector];
//    result = self.target;
    return result;
}
- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector{
    id result = [super methodSignatureForSelector:aSelector];
    NSMethodSignature *sig = [NSMethodSignature signatureWithObjCTypes:"v@:"];
    result = sig;
    return result;
    
}
- (instancetype)initWithName:(NSString *)name age:(NSUInteger)age sex:(TLSex)sex{
    if (self = [super init]) {
        _name = [name copy];
        _age = age;
        _sex = sex;
        _friends = [[NSMutableSet alloc]init];
    }
    
    return self;
}

- (id)copyWithZone:(NSZone *)zone{
    copyTest *copy = [[[self class] allocWithZone:zone] initWithName:_name age:_age sex:_sex];
    copy->_friends = [_friends mutableCopy];
    
    return copy;
}



- (id)deepCopy{
    copyTest *copy = [[[self class] alloc]initWithName:_name age:_age sex:_sex];
    copy->_friends = [[NSMutableSet alloc]initWithSet:_friends copyItems:YES];
    
    return copy;
    
}

- (void)addFriend:(copyTest *)user
{
    [_friends addObject:user];
}

- (void)removeFriend:(copyTest *)user
{
    [_friends removeObject:user];
}

- (nonnull NSString*)null{
    
    
    return @"";
    
}



@end
