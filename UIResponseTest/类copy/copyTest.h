//
//  copyTest.h
//  UIResponseTest
//
//  Created by tl on 2018/10/24.
//  Copyright © 2018年 tl. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,TLSex){
    TLMan,
    TLWoman
};

@interface copyTest : NSObject<NSCopying,NSMutableCopying>

@property(nonatomic,readonly,copy)NSString *name;
@property(nonatomic,readonly,assign)NSUInteger age;
@property(nonatomic,readonly,assign)TLSex sex;

- (instancetype)initWithName:(NSString *)name age:(NSUInteger)age sex:(TLSex)sex;
+ (instancetype)userWithName:(NSString *)name age:(NSUInteger)age sex:(TLSex)sex;

- (void)addFriend:(copyTest *)user;
- (void)removeFriend:(copyTest *)user;

- (id)deepCopy;

@end

NS_ASSUME_NONNULL_END
