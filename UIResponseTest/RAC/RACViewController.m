//
//  RACViewController.m
//  UIResponseTest
//
//  Created by tl on 2018/12/13.
//  Copyright © 2018 tl. All rights reserved.
//

#import "RACViewController.h"
#import <ReactiveObjC/ReactiveObjC.h>

@interface RACViewController ()
{
    UITextField *textField;
}

@end

@implementation RACViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    textField = [[UITextField alloc]initWithFrame:CGRectMake(100, 100, 100, 100)];
    textField.textColor = [UIColor blackColor];
    textField.placeholder = @"asdad";
    [self.view addSubview:textField];
    
    [[textField rac_signalForControlEvents:UIControlEventEditingChanged] subscribeNext:^(__kindof UIControl * _Nullable x) {
        NSLog(@"sss");
    }];
    
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"RAC" message:@"RAC TEST" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"other", nil];
    
    [[self rac_signalForSelector:@selector(alertView:clickedButtonAtIndex:) fromProtocol:@protocol(UIAlertViewDelegate)] subscribeNext:^(RACTuple *tuple) {
        
        NSLog(@"1  %@",tuple.first);
        NSLog(@"2  %@",tuple.second);
        NSLog(@"3  %@",tuple.third);
        
    }];
    [alertView show];
 
    
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:@"" object:nil] subscribeNext:^(NSNotification * _Nullable x) {
        
    }];
    
    
    [RACObserve(textField, text) subscribeNext:^(id  _Nullable x) {
        NSLog(@"cccc");
    }];
    
    UIScrollView *scrolView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 200, 400)];
    
    scrolView.contentSize = CGSizeMake(200, 800);
    scrolView.backgroundColor = [UIColor greenColor];
//    [self.view addSubview:scrolView];
    [RACObserve(scrolView, contentOffset) subscribeNext:^(id x) { NSLog(@"success"); }];
    
    
}
- (IBAction)textfChange:(id)sender {
    
    textField.text = @"change";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
