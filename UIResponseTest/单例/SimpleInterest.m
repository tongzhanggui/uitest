//
//  SimpleInterest.m
//  UIResponseTest
//
//  Created by tl on 2018/11/8.
//  Copyright © 2018年 tl. All rights reserved.
//

#import "SimpleInterest.h"

static id _instance;


@implementation SimpleInterest
- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
// 懒汉模式
//+(instancetype)allocWithZone:(struct _NSZone *)zone{
//    if (_instance == nil) {  // 防止频繁加锁
//
//        @synchronized (self) { // 加锁，避免在多线程中出错而创建多个对象
//            if (_instance == nil) {  // 防止创建多次
//                _instance = [super allocWithZone:zone];
//            }
//        }
//    }
//
//    return _instance;
//}
//
//+(instancetype)sharedSimple{
//    if (_instance == nil) {
//
//        @synchronized (self) {
//            if (_instance == nil) {
//                _instance = [[self alloc]init];
//
//            }
//        }
//    }
//
//    return _instance;
//}
//
//- (id)copyWithZone:(NSZone *)zone{
//    return _instance;
//}
//

// 饿汉模式
/**
 * 2.重写它这个类的llocWithZone:方法，这里不用加锁，因为程序刚启动，线程还没加载，不会出现线程不安全的问题
 */
//+ (instancetype)allocWithZone:(struct _NSZone *)zone{
//    if (_instance == nil) {
//
//        _instance = [super allocWithZone:zone];
//    }
//
//    return _instance;
//}
//// 3.提供一个shared方法让外界调用这个单例（一般单例都会提供这个方法）
//
//+ (instancetype)sharedSimple{
//    return _instance;
//}
//// 4.重写copyWithZone:方法，避免使用copy时创建多个对象
//- (id)copyWithZone:(NSZone *)zone{
//    return _instance;
//}
//// 5.重写load这个类方法，在里面alloc它
//// 这个方法在程序启动，加载类的时候会调用一次
//+ (void)load{
//    _instance = [[self alloc]init];
//}


// GCD
+(instancetype)allocWithZone:(struct _NSZone *)zone{
    
    static dispatch_once_t oncetoken;
    dispatch_once(&oncetoken, ^{
        _instance = [super allocWithZone:zone];
    });
    
    return _instance;
}

+ (instancetype)sharedSimple{
    
    static dispatch_once_t oncetoken;
    
    dispatch_once(&oncetoken, ^{
        _instance = [[self alloc]init];
    });
    
    return _instance;
}

- (id)copyWithZone:(NSZone *)zone{
    
    return _instance;
}






// 非ARC模式下
// 不做release
//- (oneway void)release {
//}
//
//// retain之后还是自己一份
//- (id)retain {
//    return self;
//}
//
//// 计数器永远为1
//- (NSUInteger)retainCount {
//    return 1;
//}
//
//// 防止被放进自动计数池释放
//- (id)autorelease {
//    return self;
//}

    

@end
