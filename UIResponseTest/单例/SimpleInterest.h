//
//  SimpleInterest.h
//  UIResponseTest
//
//  Created by tl on 2018/11/8.
//  Copyright © 2018年 tl. All rights reserved.
//

#import <Foundation/Foundation.h>


NS_ASSUME_NONNULL_BEGIN

@protocol SimpleInterestDelegate

- (int)add;

@end

@interface SimpleInterest : NSObject<NSCopying>

+(instancetype)sharedSimple;

@property(nonatomic,weak)id<SimpleInterestDelegate> delegate;


@end

NS_ASSUME_NONNULL_END
