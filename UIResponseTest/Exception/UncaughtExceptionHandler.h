//
//  UncaughtExceptionHanddler.h
//  UIResponseTest
//
//  Created by tl on 2019/1/8.
//  Copyright © 2019 tl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UncaughtExceptionHandler : NSObject
{
    BOOL dismissed;
}

+(void)InstallUncaughtExceptionHandler;

void UncaughtExceptionHandlers(NSException *exception);

@end

NS_ASSUME_NONNULL_END
