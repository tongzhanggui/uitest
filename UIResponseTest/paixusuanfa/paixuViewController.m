//
//  paixuViewController.m
//  UIResponseTest
//
//  Created by tl on 2018/12/10.
//  Copyright © 2018 tl. All rights reserved.
//

#import "paixuViewController.h"



//int CreatBiTree(BiTNode *root){
//    char ch = 0;
//    fflush(stdin);
//    if (ch = getchar() == 'a') {
//        *root = NULL;
//    }else{
//        *root = (BiTNode *)malloc(sizeof(BiTNode));
//        if (!(*root))
//        {
//            return RET_ERROR;
//        }
//        (*root)->data = GetRandom();
//        CreateBiTree(&(*root)->leftChild);
//        CreateBiTree(&(*root)->rightChild);
//
//    }
//
//    return RET_OK;
//}

@interface paixuViewController ()

@property(nonatomic,strong)NSMutableArray *tempArr;

@end

@implementation paixuViewController


char findFirstChar(char* cha)
{
    char result = '\0';
    // 定义一个数组 用来存储各个字母出现次数
    int array[256];
    // 对数组进行初始化操作
    for (int i=0; i<256; i++) {
        array[i] =0;
    }
    // 定义一个指针 指向当前字符串头部
    char* p = cha;
    // 遍历每个字符
    while (*p != '\0') {
        // 在字母对应存储位置 进行出现次数+1操作
        array[*(p++)]++;
    }
    
    // 将P指针重新指向字符串头部
    p = cha;
    // 遍历每个字母的出现次数
    while (*p != '\0') {
        // 遇到第一个出现次数为1的字符，打印结果
        if (array[*p] == 1)
        {
            result = *p;
            break;
        }
        // 反之继续向后遍历
        p++;
    }
    
    return result;
}



- (NSMutableArray *)tempArr{
    if (_tempArr == nil) {
        _tempArr = [NSMutableArray array];
    }
    
    return _tempArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self xuanzepaixu];
    
    
    NSMutableArray *array = @[@(22),@(55),@(23),@(93),@(23),@(4),@(56),@(1),@(34),@(69)].mutableCopy;
    
    
    [self quickSortArray:array With:0 And:array.count-1];
    
    
//    [self mergeSortArray:array lowIndex:0 highIndex:array.count-1];
    
//    [self logInsertSortArray:array];
    
    NSLog(@"%@",array);
    
    
    // Do any additional setup after loading the view.
}

#pragma mark 冒泡排序
/*
 
 冒泡排序
 
 */
- (void)maopaopaixu{
    int array[10] = {10,33,4,55,6,7,7,6,53,667};
    
    int num = sizeof(array)/sizeof(int);
    
    for (int i = 0; i<num-1; i++) {
        for (int j = 0; j<num-1-i; j++) {
//            if (array[j]<array[j+1]) {
//                int temp = array[j];
//                array[j] = array[j+1];
//                array[j+1] = temp;
//            }
            
            if (array[i]<array[j]) {
                int temp = array[j];
                array[j] = array[i];
                array[i] = temp;
            }
        }
    }
    
    for (int i = 0; i<num; i++) {
        printf("%d",array[i]);
        
        if (i == num-1) {
            printf("\n");
            
        }else{
            printf("  ");
        }
    }
}
#pragma mark 选择排序
/*
 
 选择排序
 
 */
- (void)xuanzepaixu{
    
    int array[10] = {10,33,4,55,6,7,7,6,53,667};
    
    int num = sizeof(array)/sizeof(int);
    
    for (int i = 0; i<num-1; i++) {
        for (int j = i+1; j<num; j++) {
            
            if (array[i]<array[j]) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
    }
    
    for (int i = 0; i<num; i++) {
        printf("%d",array[i]);
        
        if (i == num-1) {
            printf("\n");
            
        }else{
            printf("  ");
        }
    }
}


#pragma mark 快速排序
/*
 
 快速排序
 
 */
- (void)quickSortArray:(NSMutableArray *)array With:(NSInteger)leftIndex And:(NSInteger)rightIndex{
    
    if ((leftIndex > rightIndex) || (leftIndex == rightIndex)) {
        
        return;
    }
    
    NSInteger i = leftIndex;
    NSInteger j = rightIndex;
    
    NSInteger key = [array[i] integerValue];
    
    while (i<j) {
        
        while ((i < j) && (([array[j] integerValue] < key) || ([array[j] integerValue] == key))) {
            j --;
        }
        
        array[i] = array[j];
        
        while ((i < j) && (([array[i] integerValue] > key) || ([array[j] integerValue] == key))) {
            i ++;
        }
        
        array[j] = array[i];
    }
    
    array[i] = @(key);
    
    [self quickSortArray:array With:leftIndex And:i-1];
    [self quickSortArray:array With:i+1 And:rightIndex];
    
}

#pragma mark 归并排序
/*
 
 归并排序
 
 */
- (void)mergeSortArray:(NSMutableArray *)array lowIndex:(NSInteger)lowIndex highIndex:(NSInteger)highIndex{
    
    if (lowIndex>highIndex || lowIndex == highIndex) {
        return;
    }
    
    NSInteger midIndex  = lowIndex + (highIndex - lowIndex)/2;
    [self mergeSortArray:array lowIndex:lowIndex highIndex:midIndex];
    [self mergeSortArray:array lowIndex:midIndex+1 highIndex:highIndex];
    [self mergeArray:array lowIndex:lowIndex midIndex:midIndex highIndex:highIndex];
    
}

- (void)mergeArray:(NSMutableArray *)array lowIndex:(NSInteger)lowIndex midIndex:(NSInteger)midIndex highIndex:(NSInteger)highIndex{
    
    
    for (NSInteger i = lowIndex; i<highIndex+1; i++) {
        self.tempArr[i] = array[i];
    }
    
    NSInteger k = lowIndex;
    NSInteger l = midIndex+1;
    
    for (NSInteger j = lowIndex; j < highIndex+1; j++) {
        
        if (l > highIndex) {
            array[j] = self.tempArr[k];
            k++;
        }else if (k > midIndex){
            array[j] = self.tempArr[l];
            l++;
        }else if ([self.tempArr[k] integerValue] > [self.tempArr[l] integerValue]){
            array[j] = self.tempArr[l];
            l++;
        }else{
            array[j] = self.tempArr[k];
            k++;
        }
        
    }
    
}

#pragma mark 简单插入排序
/*
 
 简单插入排序
 
 */

- (void)logInsertSortArray:(NSMutableArray *)array{
    
    for (int i = 1; i< array.count; i++) {
        
        int j = i;
        int temp = [array[i] intValue];
        if (array[i] < array[i-1]) {
            temp = [array[i] intValue];
            while (j>0 && temp<[array[j-1] intValue]) {
                array[j] = array[j-1];
                j--;
            }
            
            array[j] = @(temp);
        }
    }
    
}

#pragma mark 堆排序 选择

#pragma mark 希尔排序 插入

#pragma mark 二分查找法






/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
