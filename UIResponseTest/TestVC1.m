//
//  TestVC1.m
//  UIResponseTest
//
//  Created by tl on 2018/11/6.
//  Copyright © 2018年 tl. All rights reserved.
//

#import "TestVC1.h"
#import <objc/runtime.h>



@interface TestVC1 ()

@end

@implementation TestVC1

- (void)viewDidLoad {
    [super viewDidLoad];

    NSString *str1 = @"1";
    NSString *str2 = [NSString stringWithFormat:@"1"];
 
     
    if ([str1 isEqual:str2]) {
        NSLog(@"1");
    }else{
        NSLog(@"2");
    }
    
    if (str1 == str2) {
        NSLog(@"==1");
    }else{
        NSLog(@"==2");
    }
    
    NSArray *arr = [NSArray array];
    
    [arr class];
    NSLog(@"%@",[arr class]);
    NSLog(@"%@",[NSArray class]);
    
    
    @try {
        NSLog(@"try");
    } @catch (NSException *exception) {
        
        NSLog(@"catch");
    } @finally {
        NSLog(@"finally");
    }
    // Do any additional setup after loading the view.
    
    if ([self respondsToSelector:@selector(aaa)]) {
        [self performSelector:@selector(aaa)];
    }
    
    
    NSLog(@"4");
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0*NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSLog(@"5");
    });
    
    
    
    NSString *appendStr = @"append";
    NSLog(@"%@",appendStr);
    NSLog(@"%@",[appendStr stringByAppendingString:@"1"]);
    NSLog(@"%@",[appendStr stringByAppendingFormat:@"1"]);
    
    TestVC1 *vc = [[TestVC1 alloc]init];
    NSLog(@"%@",vc);
    
    
    int a = [self.delegate add];
    NSLog(@"a === %d",a);
    
    
    
    
}

- (void)testBlock:(test)block{
    
    block(@"block");
}

- (void)block:(void (^)(id))block{
    
    block(@"11");
}

- (NSString *)description{
    return [NSString stringWithFormat:@"<%@: %p, %@>",[self class],self,@"==="];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
