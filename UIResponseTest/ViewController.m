//
//  ViewController.m
//  UIResponseTest
//
//  Created by tl on 2018/8/2.
//  Copyright © 2018年 tl. All rights reserved.
//

#import "ViewController.h"
#import "MoveView.h"
#import "TestView.h"
#import "Test2View.h"
#import <AFNetworking/AFNetworking.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "Masonry.h"
#import "copyTest.h"
#import <CoreText/CoreText.h>
#import "TestVC1.h"
#import "runlooptest.h"
#import "Monkey.h"

@interface ViewController ()<TestVC1Delegate>
{
    NSString *_foo;
    dispatch_queue_t _syncQueue;
}
@property(nonatomic,strong)NSString *foo;
@property(nonatomic,strong)NSString *first;
@property(nonatomic,strong)NSString *_foo;


@end

@implementation ViewController
@synthesize foo = _foo1;
@synthesize first;

- (NSString *)foo
{
    __block NSString *localSomeString;
    
    dispatch_sync(_syncQueue, ^{
        localSomeString = _foo;
    });
    
    return localSomeString;
}

- (void)setFoo:(NSString *)foo{
    dispatch_barrier_async(_syncQueue, ^{
        self->_foo = foo;
    });
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];

}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
}

// 在viewWill之后
- (void)viewWillLayoutSubviews{
    
    
}
// 在viewDid之前
- (void)viewDidLayoutSubviews{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    

    
    
    
    
    
//    NSException
    
//    NSMutableArray *arr12 = @[@"1"].mutableCopy;
//    [arr12 removeObjectAtIndex:2];
    
    NSObject *mc = [[NSObject alloc]init];
    mc = nil;
    NSAssert(mc == nil, @"我不为空了");  // 判断条件对 就过
    
    NSString *str = @"1";
    NSString *str20 = [NSString stringWithFormat:@"1"];
    if ([str isEqual:str20]) {
        
    }
    
    if (str == str20) {
        
    }
    
    
    
    NSLog(@"hash ======= %ld",str.hash);
    NSLog(@"hash ======= %ld",str20.hash);
    
    
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"plist"];
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:path];
    NSAssert(dic, @"Can't find Setting.plist or the setting file is nil!");
    
    
    NSNull *bu = [NSNull null];
    NSDictionary *d = @{@"a":bu};
    NSLog(@"null === \n%@",d);
//    if ([d[@"a"] isEqual:[NSNull null]]) {
//
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"tishi" message:nil preferredStyle:UIAlertControllerStyleAlert];
//        // 2.创建并添加按钮
//        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            NSLog(@"OK Action");
//        }];
//
//        [alert addAction:okAction];
//        // 3.呈现UIAlertContorller
//        [self presentViewController:alert animated:YES completion:nil];
//
//    }
    
    NSString *cStrArr[2][3] = {
        {d[@"a"],@"2",d},
        {@"333",@"444",@"5555"}
    };
    
    NSLog(@"%@",cStrArr[1][1]);
    
    
    _syncQueue = dispatch_queue_create("syncQueue", DISPATCH_QUEUE_CONCURRENT);
    
    
    
    
    
    
    
    NSSet *anySet = [NSSet setWithObjects:@"1",@"2",@"3",@"4",@"5", nil];
    NSLog(@"%@",[anySet allObjects]);
    
    NSString *str1 = @"1";
    NSString *str2 = [str1 mutableCopy];
    NSNumber *number = [NSNumber numberWithInt:1];
    NSString *str3 = @"2";
    NSString *str4 = @"2";
    NSLog(@"%p\n%p",str1,str2);
    
//    NSArray *arr = @[@"1",@"1",@"2",@"2",@"3"];
    NSArray *arr = @[str1,str2,str3,str4];
    NSSet *arrset = [NSSet setWithArray:arr];
    NSLog(@"%@",arrset);
    
    

    
    NSString *teststr = @"1";
    
    [self str:teststr];
    NSLog(@"%@ -- %p",teststr,teststr);
    
    
//    [self setImage];
    
   
    
    copyTest *copy1 = [[copyTest alloc]initWithName:@"1hao" age:1 sex:TLMan];
    
    copyTest *copy2 = [copy1 copy];
    copyTest *copy3 = copy1;
    
    NSLog(@"\n%p\n%p\n%p",copy1,copy2,copy3);
    
    
    _foo = @"1";
    self.foo = @"2";
    NSLog(@"foo = %@   _foo = %@",self.foo,_foo);
    _foo1 = @"3";
    NSLog(@"foo = %@   _foo = %@",self.foo,_foo);
    
    CFTypeID typeid =  CTFontGetTypeID();
    
    NSLog(@"----\n%ld",typeid);
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)str:(NSString *)str
{
//    str = @"2";
    
    NSLog(@"%@ -- %p",str,str);
}

- (void)getMethod{
    
    
    NSMutableURLRequest *requeest = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:@""] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:0.1];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
//    [NSURLSession sessionWithConfiguration:nil];
    
    
    NSURLSessionTask *task = [session dataTaskWithRequest:requeest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        
    }];
    
    
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]init];
    [manager.requestSerializer setValue:@"" forHTTPHeaderField:@""];
    
    
    [manager GET:@"" parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
    
    
    
    
    
}

- (void)setImage{
    
    UIImageView *image = [[UIImageView alloc]init];
    image.contentMode = UIViewContentModeScaleAspectFit;
//    image.image = [UIImage imageNamed:@"959-3.jpg"];
    [self.view addSubview:image];
    
    [image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY);

    }];
    
    [image sd_setImageWithURL:[NSURL URLWithString:@"http://img.hb.aicdn.com/a09289289df694cd6157f997ffa017cc44d4ca9e288fb-OehMYA_fw658"] placeholderImage:[UIImage imageNamed:@"959-3.jpg"]];
    
    
}
- (IBAction)runloop:(id)sender {
    
    runlooptest *runtest = [[runlooptest alloc]init];
    [runtest loop];
}
- (IBAction)testVC1:(id)sender {
    
    
    
    
    TestVC1 *test1 = [[TestVC1 alloc]init];

//    [self.navigationController pushViewController:test1 animated:YES];
    test1.delegate = self;

    __block int testblock = 1;
    __weak typeof(self) weakSelf = self;

    [test1 testBlock:^(id response) {
        NSLog(@"%@",response);

        __strong typeof(weakSelf) strongSelf = weakSelf;
    }];

    [test1 block:^(id res) {
        NSLog(@"%@",res);
    }];

    [self.view addSubview:test1.view];

    
   
}

- (int)add
{
    return 10;
    
}
- (IBAction)try:(id)sender {
    
    @try {
        
        NSString *s = @"1";
        [s substringFromIndex:10];
    } @catch (NSException *exception) {
        NSLog(@"%s\n%@", __FUNCTION__, exception);
        
    } @finally {
        
        NSLog(@"shixing");
    }
    
    // 认为抛出异常
//    @throw [NSException exceptionWithName:@"context show" reason:@"crush's reason" userInfo:nil];
    
    
}
- (IBAction)msgForwarding:(id)sender {
    
    Monkey *test = [[Monkey alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 全局block
void (^qiShare)(void) = ^{
    
    NSLog(@"We love sharing.");
};



@end
