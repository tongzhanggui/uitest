//
//  TLLabel.m
//  UIResponseTest
//
//  Created by tl on 2018/12/30.
//  Copyright © 2018 tl. All rights reserved.
//

#import "TLLabel.h"

@implementation TLLabel

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)labelText:(NSString *)text
      lineSpacing:(CGFloat)l_spacing
{
    if(l_spacing<0){
        self.text = text;
        return;
    }
    self.numberOfLines = 0;
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc]initWithString:text];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    
    paragraphStyle.lineSpacing = l_spacing;
    
    [attributeString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, text.length)];
    [attributeString addAttribute:NSForegroundColorAttributeName value:self.textColor range:NSMakeRange(0, text.length)];
    [attributeString addAttribute:NSFontAttributeName value:self.font range:NSMakeRange(0, text.length)];
    
    self.attributedText = attributeString;
}

- (void)labelText:(NSString *)text
   sectionSpacing:(CGFloat)s_spacing
      lineSpacing:(CGFloat)l_spacing
{
    if(s_spacing<=0 && l_spacing<=0){
        self.text = text;
        return;
    }
    
    self.numberOfLines = 0;
    
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:text];
    
    NSMutableParagraphStyle *paragrapStyle = [[NSMutableParagraphStyle alloc]init];
    
    paragrapStyle.lineSpacing = l_spacing;
    paragrapStyle.paragraphSpacing = s_spacing;

    NSRange range = NSMakeRange(0, text.length);
    NSDictionary *dict = @{NSParagraphStyleAttributeName:NSParagraphStyleAttributeName,
                           NSForegroundColorAttributeName:self.textColor,
                           NSFontAttributeName:self.font
                           };
    [attributeStr addAttributes:dict range:range];
    
    self.attributedText = attributeStr;
    
    
    
    
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
