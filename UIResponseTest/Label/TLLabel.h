//
//  TLLabel.h
//  UIResponseTest
//
//  Created by tl on 2018/12/30.
//  Copyright © 2018 tl. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TLLabel : UILabel

@end

NS_ASSUME_NONNULL_END
